import requests
from functions import count

#Initialisation du webservice 
url = "http://127.0.0.1:8000/"

    
#Création du deck
req = requests.get(url+"creer-un-deck")
deck = req.json()
print("id du deck: "+deck["deck_id"])

#Demande à l'utilisateur le nombre de cartes qu'il veut tirer, par défaut on simule le scénario
nombre_cartes = input("Combien de cartes souhaitez vous tirer ? (Sinon scénario de 10 cartes par défaut) ")

try:
    parameters = {"deck_id" : deck["deck_id"], "nombre_cartes": int(nombre_cartes)}
except:
    parameters = {"deck_id" : deck["deck_id"], "nombre_cartes": 10}

#Affichage des résultats (tirage des cartes et comptage de ceux-ci)

r = requests.post(url+"cartes/"+format(parameters["nombre_cartes"]), json=parameters)
tire = r.json()
carte = tire["cards"]
print("Liste des cartes: \n")
for i in carte:
    print(i["value"] + " of " + i["suit"])
print("\nCompte des cartes: \n")
print(count(carte))