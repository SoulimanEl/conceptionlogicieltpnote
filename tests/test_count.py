import unittest
from client.functions import count

class clientTest(unittest.TestCase):
    def test_count(self):
        cards1 = [{'code': 'KS', 'image': 'https://deckofcardsapi.com/static/img/KS.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/KS.svg', 'png': 'https://deckofcardsapi.com/static/img/KS.png'}, 'value': 'KING', 'suit': 'SPADES'},
        {'code': 'KD', 'image': 'https://deckofcardsapi.com/static/img/KD.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/KD.svg', 'png': 'https://deckofcardsapi.com/static/img/KD.png'}, 'value': 'KING', 'suit': 'DIAMONDS'},
        {'code': '6C', 'image': 'https://deckofcardsapi.com/static/img/6C.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/6C.svg', 'png': 'https://deckofcardsapi.com/static/img/6C.png'}, 'value': '6', 'suit': 'CLUBS'},
        {'code': 'QH', 'image': 'https://deckofcardsapi.com/static/img/QH.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/QH.svg', 'png': 'https://deckofcardsapi.com/static/img/QH.png'}, 'value': 'QUEEN', 'suit': 'HEARTS'},
        {'code': '0D', 'image': 'https://deckofcardsapi.com/static/img/0D.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/0D.svg', 'png': 'https://deckofcardsapi.com/static/img/0D.png'}, 'value': '10', 'suit': 'DIAMONDS'},
        {'code': '9S', 'image': 'https://deckofcardsapi.com/static/img/9S.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/9S.svg', 'png': 'https://deckofcardsapi.com/static/img/9S.png'}, 'value': '9', 'suit': 'SPADES'},
        {'code': '3D', 'image': 'https://deckofcardsapi.com/static/img/3D.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/3D.svg', 'png': 'https://deckofcardsapi.com/static/img/3D.png'}, 'value': '3', 'suit': 'DIAMONDS'},
        {'code': '5S', 'image': 'https://deckofcardsapi.com/static/img/5S.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/5S.svg', 'png': 'https://deckofcardsapi.com/static/img/5S.png'}, 'value': '5', 'suit': 'SPADES'}]

        cards2 =  [{'code': '7S', 'image': 'https://deckofcardsapi.com/static/img/7S.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/7S.svg', 'png': 'https://deckofcardsapi.com/static/img/7S.png'}, 'value': '7', 'suit': 'SPADES'}, 
        {'code': '3D', 'image': 'https://deckofcardsapi.com/static/img/3D.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/3D.svg', 'png': 'https://deckofcardsapi.com/static/img/3D.png'}, 'value': '3', 'suit': 'DIAMONDS'},
        {'code': 'KC', 'image': 'https://deckofcardsapi.com/static/img/KC.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/KC.svg', 'png': 'https://deckofcardsapi.com/static/img/KC.png'}, 'value': 'KING', 'suit': 'CLUBS'}, 
        {'code': '5H', 'image': 'https://deckofcardsapi.com/static/img/5H.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/5H.svg', 'png': 'https://deckofcardsapi.com/static/img/5H.png'}, 'value': '5', 'suit': 'HEARTS'}]

        self.assertEqual({'SPADES': 3, 'DIAMONDS': 3, 'CLUBS': 1, 'HEARTS': 1}, count(cards1))
        self.assertEqual({'SPADES': 1, 'DIAMONDS': 1, 'CLUBS': 1, 'HEARTS': 1}, count(cards2))

if __name__ == '__main__':
    unittest.main()