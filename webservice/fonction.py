from typing import Optional
from pydantic import BaseModel
import requests

#Corps métier du Webservice
class Carte(BaseModel):
    deck_id : Optional[str] = ""
    #nombre_cartes: Optional[int] = 1

#Fonction permettant de tirer des cartes
def drawCard(deck_id, nombre_cartes):
    if deck_id == "":
        r = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")  
        deck = r.json()
        deck_id = deck["deck_id"]

    req = requests.get("https://deckofcardsapi.com/api/deck/"+deck_id+"/draw/?count="+str(nombre_cartes)) 
    json=req.json()
    if "error" in json :
        return{"deck_id": json["deck_id"], "cards": json["error"]}
    else:
        return{"deck_id": json["deck_id"], "cards": json["cards"]}