import requests
from fastapi import FastAPI
import uvicorn
from fonction import *

#Initialisation du webservice avec FASTAPI
app = FastAPI()

#Message de la racine
@app.get("/")
def readroot():
    return{"On va jouer avec des cartes !!!"}

#Création d'un deck sur creer-un-deck
@app.get("/creer-un-deck/")
def get_idDeck():
    requete = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")  
    rjson = requete.json()
    return{"deck_id" : rjson["deck_id"]}


#Tirage des cartes sur cartes/nombres_cartes
@app.post("/cartes/{nombre_cartes}")
def tirercarte(c : Carte, nombre_cartes : int):
    res = drawCard(c.deck_id, nombre_cartes)  
    return(res)
    