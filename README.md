# :hearts: :spades: :diamonds: :clubs: TP noté Conception Logiciel  :clubs: :diamonds: :spades: :hearts:   

Les commandes présentées ici sont à lancer depuis un terminal :

## Webservice :

Depuis la racine :
```
$ cd webservice
$ pip install -r requirements.txt
$ uvicorn main:app --reload
```
Enfin, on accède à l'API via ce lien : http://127.0.0.1:8000/docs#


## Client :

Une fois le Webservice lancé comme présenté ci-dessus, il faut ouvrir un second terminal puis, depuis la racine :
```
$ cd client
$ pip install -r requirements.txt
$ python main.py ou $ python3 main.py
```
**Pour le comptage des cartes, en ne rentrant aucune valeur lorsque le nombre de carte à tirer est demandé, un scénario à 10 cartes est pris par défaut.**

## Tests unitaires

Un test est fait sur le comptage des cartes de la partie Client. Pour lancer le test depuis la racine :

`$ python -m unittest tests/test_count.py ou $ python3 -m unittest tests/test_count.py`


# :hearts: :spades: :diamonds: :clubs: Et voilà, bonne utilisation !  :clubs: :diamonds: :spades: :hearts:   
